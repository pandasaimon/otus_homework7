
function Table(data) {
	data = data.data;
	return ( data != null &&
	  (<table className="table table-striped" aria-labelledby="tableLabel">
		<thead>
		  <tr>
			<th>Date</th>
			<th>Temp. (C)</th>
			<th>Temp. (F)</th>
			<th>Summary</th>
		  </tr>
		</thead>
		<tbody>
		  {data.map(forecast =>
			<tr key={forecast.date}>
			  <td>{forecast.date}</td>
			  <td>{forecast.temperatureC}</td>
			  <td>{forecast.temperatureF}</td>
			  <td>{forecast.summary}</td>
			</tr>
		  )}
		</tbody>
	  </table>)
	);
  }
  
  export default Table;