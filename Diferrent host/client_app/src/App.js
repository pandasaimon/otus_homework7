import React, { useState } from 'react';
import axios from 'axios';
import './App.css';
import Table from './Table';
import RequestHelper from './RequestHelper'

function App() {
  const [data, setData] = useState(null);
  fetchRequest(setData);
  return (
    <Table data={data}/>
  );
}


async function fetchRequest(setData) {
    setData(await RequestHelper.fetchRequest("http://localhost:5186/WeatherForecast"));
}

export default App;
