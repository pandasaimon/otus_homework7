import axios from 'axios';

export default class RequestHelper {

    static async fetchRequest(inputUrlText) {
        try {
            let response = await axios.put(inputUrlText);
            return response.data
        } catch (err) {
            return null;
        }
    }
}